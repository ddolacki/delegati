﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delegati
{
    class Pistolj
    {
        private const int n = 6;
        private int polozaj;
        int[] bubanj = new int[n];
        Random rand = new Random();
        public delegate void PucanjPistolja(string poruka);
        //public delegate void Delegat(int[] bubanj);
        public event PucanjPistolja Bang;

        public Pistolj() {

            for (int i = 0; i < n; i++) {
                bubanj[i] = 0;
            }
            int j = rand.Next(0, 5);
            bubanj[j] = 1;            
        } 

        public void Potegni(){
            if (bubanj[polozaj] == 1) {
                Bang?.Invoke("opalio sam");
            }
            polozaj = (polozaj + 1) % 6;
        }




    }
}
