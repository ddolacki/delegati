﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delegati
{
    class Program
    {
        static void Main(string[] args)
        {
            Pistolj ak47 = new Pistolj();
            ak47.Bang += Dogadjaj;

            for (int i = 0; i < 12; i++) {
                Console.WriteLine("Probavam...");
                ak47.Potegni();
            }


            //Delegate delegat = new Delegate(Pokazi);
            //delegat(""); 
        }

        static void Dogadjaj(string nest)
        {
            Console.WriteLine(nest);
        }
    }
}
